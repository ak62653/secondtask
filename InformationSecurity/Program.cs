﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

class Program
{
    static void Main()
    {
        string textmessage;
        string key;

        bool continueExecution = true;

        while (continueExecution)
        {
            Console.WriteLine("Enter textmessage:");
            textmessage = Console.ReadLine();

            Console.WriteLine("Enter key:");
            key = Console.ReadLine();

            key = key.Length < 16 ? key.PadRight(16, '*') :
                        key.Length < 24 ? key.PadRight(24, '*') :
                        key.Length > 32 ? key.Substring(0, 32) : key;

            bool decryptSameText = true;

            do
            {
                Console.WriteLine("Select encryption/decryption function:");
                Console.WriteLine("1. Encrypt");
                Console.WriteLine("2. Decrypt");
                int choice = int.Parse(Console.ReadLine());

                Console.WriteLine("Choose block cipher mode of operation:");
                Console.WriteLine("1. ECB");
                Console.WriteLine("2. CBC");
                Console.WriteLine("3. CFB");
                CipherMode mode = CipherMode.ECB;
                int modeChoice = int.Parse(Console.ReadLine());
                switch (modeChoice)
                {
                    case 1:
                        mode = CipherMode.ECB;
                        break;
                    case 2:
                        mode = CipherMode.CBC;
                        break;
                    case 3:
                        mode = CipherMode.CFB;
                        break;
                    default:
                        Console.WriteLine("Invalid choice. Defaulting to ECB.");
                        break;
                }

                byte[] encryptedBytes;
                byte[] IV = null;

                if (choice == 1)
                {

                    IV = GenerateRandomIV();
                    encryptedBytes = EncryptStringToBytes_Aes(textmessage, key, IV, mode);
                    File.WriteAllBytes("encrypted.txt", IV.Concat(encryptedBytes).ToArray());
                    Console.WriteLine("Encrypted text saved to encrypted.txt");
                    decryptSameText = true;
                }
                else if (choice == 2)
                {
                    byte[] cipherTextWithIV = File.ReadAllBytes("encrypted.txt");
                    IV = cipherTextWithIV.Take(16).ToArray();
                    byte[] cipherText = cipherTextWithIV.Skip(16).ToArray();
                    string decryptedText = DecryptStringFromBytes_Aes(cipherText, key, IV, mode);
                    Console.WriteLine("Decrypted text: {0}", decryptedText);
                    decryptSameText = false;
                }
                else
                {
                    Console.WriteLine("Invalid choice.");
                }
            } while (decryptSameText);

            Console.WriteLine("Do you want to continue? (yes/no)");
            string continueChoice = Console.ReadLine().ToLower();
            continueExecution = continueChoice == "yes";
        }
    }

    static byte[] EncryptStringToBytes_Aes(string textmessage, string Key, byte[] IV, CipherMode mode)
    {
        byte[] encrypted;
        using (Aes aesAlg = Aes.Create())
        {
            aesAlg.Key = Encoding.UTF8.GetBytes(Key);
            aesAlg.IV = IV;
            aesAlg.Mode = mode;
            aesAlg.Padding = PaddingMode.PKCS7;

            ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                    {
                        swEncrypt.Write(textmessage);
                    }
                }
                encrypted = msEncrypt.ToArray();
            }
        }
        return encrypted;
    }

    static string DecryptStringFromBytes_Aes(byte[] cipherText, string Key, byte[] IV, CipherMode mode)
    {
        string textmessage = null;
        using (Aes aesAlg = Aes.Create())
        {
            aesAlg.Key = Encoding.UTF8.GetBytes(Key);
            aesAlg.IV = IV;
            aesAlg.Mode = mode;
            aesAlg.Padding = PaddingMode.PKCS7;

            ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

            using (MemoryStream msDecrypt = new MemoryStream(cipherText))
            {
                using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                {
                    using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                    {
                        textmessage = srDecrypt.ReadToEnd();
                    }
                }
            }
        }
        return textmessage;
    }

    static byte[] GenerateRandomIV()
    {
        using (Aes aes = Aes.Create())
        {
            aes.GenerateIV();
            return aes.IV;
        }
    }
}

